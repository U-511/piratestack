﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseGameManager : MonoBehaviour
{
    public static BaseGameManager singleton;
    public Vector3 cameraPos;

    private void Awake()
    {
        singleton = this;
    }

    public void GameOverPopup(int score)
    {
        //Popup GameOver window in UIManager(fading + score calculation)
        //Save player best score if player bit it
        UIManager.singleton.OpenDeathWindow(PlayerPrefs.GetInt("Best"));
        SaveScore(score);
    }

    private void Start()
    {
        TowerColorManager.singleton.ColorizeGrad();
        //Open menu, update info on it
        BlockTowerManager.singleton.Startup();


        UIManager.singleton.OpenMainMenuWindow(PlayerPrefs.GetInt("Best"), TowerColorManager.singleton.GetColor(0));
        cameraPos = Camera.main.gameObject.transform.position;
    }

    public void Play()
    {
        BlockTowerManager.singleton.StartMech();
        UIManager.singleton.CloseMainMenuWindow();
        UIManager.singleton.OpenGameScore();
    }

    public void Restart()
    {
        TowerColorManager.singleton.ColorizeGrad();
        BlockTowerManager.singleton.DestoyTower();

        BlockTowerManager.singleton.Startup();
        UIManager.singleton.CloseMainMenuWindow();
        UIManager.singleton.CloseDeathWindow();
        UIManager.singleton.OpenGameScore();
        Play();
    }

    public void ToMainMenu()
    {
        UIManager.singleton.CloseDeathWindow();
        TowerColorManager.singleton.ColorizeGrad();
        //Open menu, update info on it
        BlockTowerManager.singleton.DestoyTower();

        BlockTowerManager.singleton.Startup();


        UIManager.singleton.OpenMainMenuWindow(PlayerPrefs.GetInt("Best"), TowerColorManager.singleton.GetColor(0));
    }

    public void SaveScore(int score)
    {
        int bestScore = PlayerPrefs.GetInt("Best");
        if (bestScore < score)
        {
            PlayerPrefs.SetInt("Best", score);
        }

    }

    public void Exit()
    {
        Application.Quit();
    }

    public void NullPlayerPrefs()
    {
        PlayerPrefs.DeleteAll();
    }
}
