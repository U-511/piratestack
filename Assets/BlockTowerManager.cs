﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockTowerManager : MonoBehaviour
{
    public static BlockTowerManager singleton;

    public GameObject blockPref;
    public float blockHeight;
    public Vector3 lastBlockSize;
    public Vector3 lastBlockPos;
    [Space]
    public int score;
    [Space]
    public int colorInRow;
    public int colorRowCount;
    [Space]
    public GameObject checkBlock;
    public const float startSpeed = 30f;
    public float speed;

    public GameObject towerParent;
    public Vector2 maxPosOffset;
    public bool isZAxis = false;

    public bool mechInit;

    private void Awake()
    {
        singleton = this;
    }

    public void Startup()
    {
        //Place foundation block
        //Fill last block size and block scale
        //Set color grad
        speed = startSpeed;
        score = 0;

        GameObject block = Instantiate(blockPref);
        colorInRow = 0;
        colorInRow++;
        TowerColorManager.singleton.SetColorToObject(block, (float)colorInRow / colorRowCount);
        CameraScript.singleton.UpdateColor((float)colorInRow / colorRowCount);

        block.transform.parent = towerParent.transform;

        block.transform.position = Vector3.zero;
        lastBlockSize = block.transform.localScale;
        lastBlockPos = block.transform.position;

        UIManager.singleton.UpdateScore(score);
        UIManager.singleton.UpdateOutLineColor(TowerColorManager.singleton.GetColor((float)colorInRow / colorRowCount));

        
    }

    public void StartMech()
    {
        mechInit = true;
        SpawnCube();
    }

    public void DestoyTower()
    {
        int childCount = towerParent.transform.childCount;
        CameraScript.singleton.RepositionCall(BaseGameManager.singleton.cameraPos);

        if (childCount <= 0)
            return;

        for (int i = 0; i < childCount; i++)
        {
            Destroy(towerParent.transform.GetChild(i).gameObject);
        }
    }

    public void SpawnCube()
    {
        checkBlock = Instantiate(blockPref);
        Vector3 pos = Vector3.zero;
        //Based on axis add to cube different trust vector
        //Save this block and wait for player tap
        checkBlock.transform.localScale = lastBlockSize;
        if (isZAxis)
        {
            pos = new Vector3(lastBlockPos.x, lastBlockPos.y + blockHeight,  (-2f * lastBlockSize.z + (lastBlockPos.z)));
        }
        else
        {
            pos = new Vector3((-2f * lastBlockSize.x + (lastBlockPos.x)), lastBlockPos.y + blockHeight, lastBlockPos.z);
        }
        checkBlock.transform.position = pos;

        TowerColorManager.singleton.SetColorToObject(checkBlock, (float)colorInRow / colorRowCount);
        CameraScript.singleton.UpdateColor((float)colorInRow / colorRowCount);
        //Connect Dotween or translate by local script
        //Let block go and wait untill push
    }

    private void FixedUpdate()
    {
        if (mechInit == false || checkBlock == null)
            return;

        if (checkBlock != null)
        {
            if (isZAxis)
            {
                checkBlock.transform.Translate(Vector3.forward * Time.fixedDeltaTime * speed);
            }
            else
            {
                checkBlock.transform.Translate(Vector3.right * Time.fixedDeltaTime * speed);
            }
        }

        //If block != null wait untill player push
        //if player not pushed untill block pos > last block pos+scale*2
        //Block will be destroyed and player will get gameOver
        if (checkBlock != null)
        {
            if (CheckBlockPos() == false)
            {
                //Block is out of bounds, destroy it 
                //Spawn new block OR popup LoseScreen
                Destroy(checkBlock);
    
                checkBlock = null;
                BaseGameManager.singleton.GameOverPopup(score);
                mechInit = false;
                return;
            }

            if (CheckBlockSize() == false)
            {
                Destroy(checkBlock);
                checkBlock = null;
                BaseGameManager.singleton.GameOverPopup(score);
                mechInit = false;

                return;
            }
        }

        
        
    }

    private void Update()
    {
        if (mechInit == false || checkBlock == null)
            return;
        //Check if push, based on current block pos check
        //Or it will stay at last block pos + 20y without any changes
        //Or block will be cutted
        if (Input.GetMouseButtonDown(0))
        {
            PlaceBlock();
            return;
        }
    }

    //Save start data of block, it place and size
    //Place blocks and calculate what will be cut and what will stay

    //After drop a block start calculating
    //Block position == local pos / 2
    //Block scale = block size - local pos
    //Spawn cuted out block
    //Blocks size = block size - blockScale
    //block pos = block pos * 2 + block size / 2
    //Add to block rigid body
    //Time to destroy

    public bool CheckBlockPos()
    {
        float maxDistance = 0;
        if (isZAxis)
            maxDistance = Mathf.Abs(lastBlockPos.z) + lastBlockSize.z * 1.5f;
        else
            maxDistance = Mathf.Abs(lastBlockPos.x) + lastBlockSize.x * 1.5f;

        maxDistance *= 3f;
        maxDistance = Mathf.Abs(maxDistance);

        Vector3 tempBlockPos = new Vector3(lastBlockPos.x, lastBlockPos.y + blockHeight, lastBlockPos.z);
        if (Vector3.Distance(tempBlockPos, checkBlock.transform.position) > maxDistance)
        {
            Debug.LogError("Blocks out of distance");
            return false;
        }
        else
        {
            return true;
        }
    }

    public bool CheckBlockSize()
    {
        if (mechInit == false)
            return false;
        //Check if scale == 0 or < 0
        //if scale == 0 or < 0 then popup lose screen
        if (checkBlock.transform.localScale.x <= 0 || checkBlock.transform.localScale.z <= 0)
        {
            Debug.LogError("Blocks out of scale, LOSE");
            return false;
        }
        return true;
    }

    public void PlaceBlock()
    {
        if (mechInit == false)
            return;

        if (CheckBlockSize() == false)
        {
            BaseGameManager.singleton.GameOverPopup(score);
            return;
        }
            
        //Check if block size is not out of bounds, if it is
        //Destroy block, spawn new or popup LoseScreen
        float maxDistance = 0;
        if (isZAxis)
            maxDistance = Mathf.Abs(lastBlockPos.z) + lastBlockSize.z;
        else
            maxDistance = Mathf.Abs(lastBlockPos.x) + lastBlockSize.x;

        maxDistance = Mathf.Abs(maxDistance);

        Vector3 tempBlockPos = new Vector3(lastBlockPos.x, lastBlockPos.y + blockHeight, lastBlockPos.z);
        if (Vector3.Distance(tempBlockPos, checkBlock.transform.position) > maxDistance)
        {
            Debug.LogError("Block is out of size");
            Destroy(checkBlock);
            //isZAxis = !isZAxis;
            //SpawnCube();
            BaseGameManager.singleton.GameOverPopup(score);
            return;
        }

        //Check if block is in offset area, if true
        //Set block pos in offset area 
        //Just place it in last position +block height by y


        //if block is out of offset area start calculating on 
        //right type to cut out of bounds area
        

        Vector3 chippedBlockPos;
        bool hasChippedBlock = false;
        if (isZAxis)
        {
            //Set block new size
            float axisOffset = checkBlock.transform.position.z - tempBlockPos.z;
            Debug.Log("Axis offset is:" + axisOffset + " last blockPos:" + tempBlockPos + " new blockPos:" + checkBlock.transform.position);
            if (Mathf.Abs(axisOffset) < lastBlockSize.z * maxPosOffset.y)
            {
                checkBlock.transform.position = new Vector3(lastBlockPos.x, lastBlockPos.y + blockHeight, lastBlockPos.z);
            }
            else
            {
                //blockSize = Mathf.Abs(blockSize);
                checkBlock.transform.position = new Vector3(checkBlock.transform.position.x, checkBlock.transform.position.y, lastBlockPos.z + axisOffset / 2);

                Vector3 scale = checkBlock.transform.localScale;
                scale.z -= Mathf.Abs(axisOffset);
                checkBlock.transform.localScale = scale;
                hasChippedBlock = true;
            }

            //Spawn chipped block
            if (hasChippedBlock == true)
            {
                GameObject chippedBlock = Instantiate(blockPref);
                chippedBlock.GetComponent<MeshRenderer>().material = checkBlock.GetComponent<MeshRenderer>().material;

                if (axisOffset > 0)
                {
                    chippedBlockPos = new Vector3(checkBlock.transform.position.x, checkBlock.transform.position.y, 
                        lastBlockPos.z + axisOffset + checkBlock.transform.localScale.z / 2);
                }
                else
                {
                    chippedBlockPos = new Vector3(checkBlock.transform.position.x, checkBlock.transform.position.y, 
                        (lastBlockPos.z + axisOffset - checkBlock.transform.localScale.z / 2));

                }
                //
                chippedBlock.transform.position = chippedBlockPos;
                chippedBlock.transform.localScale = new Vector3(checkBlock.transform.localScale.x, checkBlock.transform.localScale.y, Mathf.Abs(lastBlockSize.z - checkBlock.transform.localScale.z));
                Rigidbody rgd = chippedBlock.AddComponent<Rigidbody>();
                chippedBlock.name = "ChippedBlock";
                rgd.mass = 100;

                Destroy(chippedBlock, 10f);
            }
            
        }
        else
        {
            float axisOffset = checkBlock.transform.position.x - tempBlockPos.x;
            Debug.Log("Axis offset is:" + axisOffset + " last blockPos:" + tempBlockPos + " new blockPos:" + checkBlock.transform.position);
            

            if (Mathf.Abs(axisOffset) < lastBlockSize.x * maxPosOffset.x)
            {
                checkBlock.transform.position = new Vector3(lastBlockPos.x, lastBlockPos.y + blockHeight, lastBlockPos.z);
            }
            else
            {
                //blockSize = Mathf.Abs(blockSize);
                checkBlock.transform.position = new Vector3(lastBlockPos.x + axisOffset / 2, checkBlock.transform.position.y, checkBlock.transform.position.z);

                Vector3 scale = checkBlock.transform.localScale;
                scale.x -= Mathf.Abs(axisOffset);
                checkBlock.transform.localScale = scale;
                hasChippedBlock = true;
            }


            //Spawn chipped block
            if (hasChippedBlock == true)
            {
                GameObject chippedBlock = Instantiate(blockPref);
                chippedBlock.GetComponent<MeshRenderer>().material = checkBlock.GetComponent<MeshRenderer>().material;
                if (axisOffset > 0)
                {
                    chippedBlockPos = new Vector3(lastBlockPos.x + axisOffset + checkBlock.transform.localScale.x / 2,
                        checkBlock.transform.position.y, checkBlock.transform.position.z);
                }
                else
                {
                    chippedBlockPos = new Vector3(lastBlockPos.x + axisOffset - checkBlock.transform.localScale.x / 2,
                        checkBlock.transform.position.y, checkBlock.transform.position.z);

                }
                //
                chippedBlock.transform.position = chippedBlockPos;
                chippedBlock.transform.localScale = new Vector3(Mathf.Abs(lastBlockSize.x - checkBlock.transform.localScale.x),
                    checkBlock.transform.localScale.y, checkBlock.transform.localScale.z);
                Rigidbody rgd = chippedBlock.AddComponent<Rigidbody>();
                chippedBlock.name = "ChippedBlock";
                rgd.mass = 100;

                Destroy(chippedBlock, 10f);
            }
        }

        checkBlock.transform.parent = towerParent.transform;

        lastBlockSize = checkBlock.transform.localScale;
        lastBlockPos = checkBlock.transform.position;
        checkBlock = null;
        isZAxis = !isZAxis;
        colorInRow++;
        score++;
        
        if (colorInRow == colorRowCount)
        {
            colorInRow = 0;
            //Apply new speed
            //Create new gradient
            speed += 5;
            TowerColorManager.singleton.ColorizeGrad(TowerColorManager.singleton.mainGrad.Evaluate(1));
        }
        UIManager.singleton.UpdateScore(score);
        UIManager.singleton.UpdateOutLineColor(TowerColorManager.singleton.GetColor((float)colorInRow / colorRowCount));
        CameraScript.singleton.RepositionCall(blockHeight);

        SpawnCube();
    }
}
