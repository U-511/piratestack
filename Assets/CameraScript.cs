﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{
    public static CameraScript singleton;
    public Coroutine cameraCor;
    public Coroutine cameraColorCor;
    public int bgColorDrop;
    [Space]
    public float timeToGradiate;
    public float desaturationGrade;
    [Space]
    public float repositionTime;

    private void Awake()
    {
        singleton = this;
    }

    public void RepositionCall(float newPositionY)
    {
        Vector3 callPos = gameObject.transform.position;
        Vector3 newPos = new Vector3(callPos.x, callPos.y + newPositionY, callPos.z);
        if (cameraCor != null)
        {
            StopCoroutine(cameraCor);
        }
        cameraCor = StartCoroutine(Reposition(newPos, repositionTime));
    }

    public void RepositionCall(Vector3 newPosition)
    {
        if (cameraCor != null)
        {
            StopCoroutine(cameraCor);
        }
        cameraCor = StartCoroutine(Reposition(newPosition, repositionTime));
    }

    public IEnumerator Reposition(Vector3 newPos, float pingTime = 1)
    {
        float rate = 1.0f / pingTime;
        float progress = 0.0f;
        Vector3 startPos = gameObject.transform.position;
       
        while (progress < 1.0)
        {
            gameObject.transform.position = Vector3.Lerp(startPos, newPos, progress);

            progress += rate * Time.deltaTime;
            yield return null;
        }
        gameObject.transform.position = newPos;
        cameraCor = null;
        yield break;
    }

    public void UpdateColor(float gradProg)
    {
        Color32 color = TowerColorManager.singleton.GetColor(gradProg);
        //Start bumping
        byte red = color.r;
        byte green = color.g;
        byte blue = color.b;

        red = TowerColorManager.Damper(red, bgColorDrop);
        green = TowerColorManager.Damper(green, bgColorDrop);
        blue = TowerColorManager.Damper(blue, bgColorDrop);

        color.r = red;
        color.g = green;
        color.b = blue;

        color = TowerColorManager.Desaturation(color, desaturationGrade);

        //Camera.main.backgroundColor = color;
        if (cameraColorCor != null)
        {
            StopCoroutine(cameraColorCor);
        }
        cameraColorCor = StartCoroutine(Recolor(color));
    }

    //public byte Damper(int origin, int dump)
    //{
    //    if (origin - dump < 0)
    //    {
    //        origin = 0;
    //        return (byte)origin;
    //    }

    //    return (byte)origin;
    //}

    public IEnumerator Recolor(Color32 targetColor)
    {
        float rate = 1.0f / timeToGradiate;
        float progress = 0.0f;

        Color32 startColor = Camera.main.backgroundColor;

        while (progress < 1.0)
        {
            Color32 color = new Color32(0,0,0,255);

            color.r = (byte)Mathf.Lerp(startColor.r, targetColor.r, progress);
            color.g = (byte)Mathf.Lerp(startColor.g, targetColor.g, progress);
            color.b = (byte)Mathf.Lerp(startColor.b, targetColor.b, progress);

            //Debug.Log("Progress:" + progress);
            progress += rate * Time.deltaTime;
            Camera.main.backgroundColor = color;
            yield return null;
        }
        Camera.main.backgroundColor = targetColor;
        cameraColorCor = null;
        yield break;
    }
}
