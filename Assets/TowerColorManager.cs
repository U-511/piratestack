﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerColorManager : MonoBehaviour
{
    public Gradient mainGrad;
    public int colorValueMin;
    public int colorValueMax;

    public const float Pr = 0.299f;
    public const float Pg = 0.587f;
    public const float Pb = 0.114f;

    public static TowerColorManager singleton;

    private void Awake()
    {
        singleton = this;
    }

    /// <summary>
    /// Generate full color gradient
    /// </summary>
    /// <returns></returns>
    public void ColorizeGrad()
    {
        Gradient grad = new Gradient();

        GradientColorKey[] colorKeys = new GradientColorKey[2];
        GradientAlphaKey[] alphaKeys = new GradientAlphaKey[2];
        byte red = (byte)Random.Range(0, 255);
        byte green = (byte)Random.Range(0, 255);
        byte blue = (byte)Random.Range(0, 255);

        int TotalValue = red + green + blue;
        while (TotalValue < colorValueMin || TotalValue > colorValueMax)
        {
            red = (byte)Random.Range(0, 255);
            green = (byte)Random.Range(0, 255);
            blue = (byte)Random.Range(0, 255);
            TotalValue = red + green + blue;
        }

        Color32 mainColor = new Color32(red, green, blue, 255);

        TotalValue = 0;
        while (TotalValue < colorValueMin || TotalValue > colorValueMax)
        {
            red = (byte)Random.Range(0, 255);
            green = (byte)Random.Range(0, 255);
            blue = (byte)Random.Range(0, 255);
            TotalValue = red + green + blue;
        }
        Color32 secondColor = new Color32(red, green, blue, 255);

        colorKeys[0] = new GradientColorKey(mainColor, 0);
        alphaKeys[0] = new GradientAlphaKey(1, 0);

        colorKeys[1] = new GradientColorKey(secondColor, 1);
        alphaKeys[1] = new GradientAlphaKey(1, 1);

        grad.SetKeys(colorKeys, alphaKeys);

        mainGrad = grad;
    }

    /// <summary>
    /// Generate gradient, where first color == start color, second color is random
    /// </summary>
    /// <param name="startColor"></param>
    /// <returns></returns>
    public void ColorizeGrad(Color32 startColor)
    {
        Gradient grad = new Gradient();

        GradientColorKey[] colorKeys = new GradientColorKey[2];
        GradientAlphaKey[] alphaKeys = new GradientAlphaKey[2];
        byte red = (byte)Random.Range(0, 255);
        byte green = (byte)Random.Range(0, 255);
        byte blue = (byte)Random.Range(0, 255);

        int TotalValue = red + green + blue;
        //while (TotalValue < colorValueMin || TotalValue > colorValueMax)
        //{
        //    red = (byte)Random.Range(0, 255);
        //    green = (byte)Random.Range(0, 255);
        //    blue = (byte)Random.Range(0, 255);
        //    TotalValue = red + green + blue;
        //}

        //Color32 mainColor = new Color32(red, green, blue, 255);

        TotalValue = 0;
        while (TotalValue < colorValueMin || TotalValue > colorValueMax)
        {
            red = (byte)Random.Range(0, 255);
            green = (byte)Random.Range(0, 255);
            blue = (byte)Random.Range(0, 255);
            TotalValue = red + green + blue;
        }
        Color32 secondColor = new Color32(red, green, blue, 255);

        colorKeys[0] = new GradientColorKey(startColor, 0);
        alphaKeys[0] = new GradientAlphaKey(1, 0);

        colorKeys[1] = new GradientColorKey(secondColor, 1);
        alphaKeys[1] = new GradientAlphaKey(1, 1);

        grad.SetKeys(colorKeys, alphaKeys);

        mainGrad = grad;
    }

    public void SetColorToObject(GameObject _target, float gradINN)
    {
        _target.GetComponent<MeshRenderer>().material.color = mainGrad.Evaluate(gradINN);
    }

    public Color32 GetColor(float gradInn)
    {
        return mainGrad.Evaluate(gradInn);
    }


    public static byte Damper(int origin, int dump)
    {
        if (origin - dump < 0)
        {
            origin = 0;
            return (byte)origin;
        }

        return (byte)origin;
    }

    public static Color32 Desaturation(Color32 origin, float change)
    {
        byte Red = origin.r;
        byte Green = origin.g;
        byte Blue = origin.b;

        double P = Mathf.Sqrt(
        (Red) * (Red) * Pr +
        (Green) * (Green) * Pg +
        (Blue) * (Blue) * Pb);

        Red = (byte)(P + ((Red) - P) * change);
        Green = (byte)(P + ((Green) - P) * change);
        Blue = (byte)(P + ((Blue) - P) * change);

        origin.r = Red;
        origin.g = Green;
        origin.b = Blue;

        return origin;
    }
}
