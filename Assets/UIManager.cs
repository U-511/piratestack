﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [Header("Game Screen")]
    public Text score;
    public Outline scoreOutline;

    [Header("Game OverScreen")]
    public GameObject gameOverScreen;
    [Space]
    public Outline gameOverLabel;
    public float scoreRewriteTime;
    public Text gameOverScore;
    public Text bestScoreText;
    public Image crownIcon;

    [Header("MainMenuScene")]
    public GameObject gameMainMenu;
    [Space]
    public Outline labelOutline;
    public Text bestScoreTxt;

    [Space]
    public int bestScore = 0;
    [Space]
    public float fadingTime;
    //Update Back Ground
    //Update Score,
    //Update color of score text

    //Cotain data about windows, open or close on call

    public static UIManager singleton;

    private void Awake()
    {
        singleton = this;
    }

    public void UpdateScore(int _score)
    {
        score.text = _score.ToString();
    }

    public void UpdateOutLineColor(Color32 color)
    {
        scoreOutline.effectColor = color;
    }

    public void OpenDeathWindow(int _bestScore)
    {
        bestScore = _bestScore;
        FadingUITarget(false, score.gameObject);
        FadingUITarget(true, gameOverScreen);
        int inRow = BlockTowerManager.singleton.colorInRow;
        int count = BlockTowerManager.singleton.colorRowCount;
        gameOverLabel.effectColor = TowerColorManager.singleton.GetColor((float)inRow / count);

        bestScoreText.text = "Best: " + _bestScore;
        StartCoroutine(ScoreReWrite(BlockTowerManager.singleton.score));
    }

    public void CloseDeathWindow()
    {
        FadingUITarget(false, gameOverScreen);
    }

    public void OpenMainMenuWindow(int _bestScore, Color32 _outLineColor)
    {
        FadingUITarget(false, score.gameObject);
        FadingUITarget(false, gameOverScreen);

        bestScoreTxt.text = "Best Score: " + _bestScore;
        labelOutline.effectColor = _outLineColor;

        FadingUITarget(true, gameMainMenu);
    }

    public void CloseMainMenuWindow()
    {
        FadingUITarget(false, gameMainMenu);
    }

    public void OpenGameScore()
    {
        FadingUITarget(true, score.gameObject);
    }

    public IEnumerator ScoreReWrite(int targetScore)
    {
        float rate = 1.0f / scoreRewriteTime;
        float progress = 0.0f;

        Color32 startColor = Camera.main.backgroundColor;

        while (progress < 1.0)
        {
            int tempScore = Mathf.RoundToInt(Mathf.Lerp(0, targetScore, progress));

            //Debug.Log("Progress:" + progress);
            progress += rate * Time.deltaTime;

            gameOverScore.text = "Score: " + tempScore;

            if (tempScore > bestScore)
            {
                gameOverScore.text = GoldieText(tempScore.ToString());
                crownIcon.gameObject.SetActive(true);
            }
            else
                crownIcon.gameObject.SetActive(false);



            yield return null;
        }
        yield break;
    }

    public void FadingUITarget(bool open, GameObject target)
    {
        if (open == true)
            target.SetActive(open);

        StartCoroutine(FadingUITargetCor(open, target));
    }

    public IEnumerator FadingUITargetCor(bool open, GameObject target)
    {
        CanvasGroup targetGroup = target.GetComponent<CanvasGroup>();
        float rate = 1.0f / scoreRewriteTime;
        float progress = 0.0f;

        float startState = targetGroup.alpha;
        float endState;
        if (open)
            endState = 1;
        else
            endState = 0;

        while (progress < 1.0)
        {
            float tempAlpfa = Mathf.RoundToInt(Mathf.Lerp(startState, endState, progress));

            //Debug.Log("Progress:" + progress);
            progress += rate * Time.deltaTime;

            targetGroup.alpha = tempAlpfa;
            yield return null;
        }

        if (open == false)
            target.SetActive(false);

        yield break;
    }

    public string GoldieText(string text)
    {
        string s = string.Format("<color=#FFD700>{0}</color>", "Score:" + text);
        return s;
    }
}
